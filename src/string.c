#include <odweta/string.h>
#include <odweta/util.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

size_t chars_length(char *chars) {
    int i = 0;
    while (*(chars + (i++)) != '\0');

    return i-1;
}




string_t *string_new() {
    string_t *s = (string_t *)malloc(sizeof(string_t));
    if (s == NULL) {
        return NULL;
    }

    s->content = (char *)malloc(sizeof(char));
    if (s->content == NULL) {
        free(s); // Free the previously allocated memory for string_t structure
        return NULL;
    }

    *(s->content) = '\0';  // Initialize to empty string
    s->length = 0;

    return s;
}

string_t *string_from(char *chars) {
    // Allocate memory for the string_t structure
    string_t *s = (string_t *)malloc(sizeof(string_t));
    if (s == NULL) {
        return NULL;
    }

    // Calculate the length of the input string
    int len = chars_length(chars);

    // Allocate memory for the content
    s->content = (char *)malloc((len + 1) * sizeof(char));  // Add 1 for null terminator
    if (s->content == NULL) {
        free(s); // Free previously allocated memory for string_t structure
        return NULL;
    }

    // Copy the characters
    for (int i = 0; i < len; i++) {
        s->content[i] = chars[i];
    }

    // Null-terminate the string
    s->content[len] = '\0';

    // Update the length
    s->length = len;

    return s;
}

void string_deep_copy(string_t *src, string_t* dst) {
    if (src == NULL) {
        dst = NULL;
        return;
    }

    // Allocate memory for the new string
    string_t *dest = string_new();
    if (dest == NULL) {
        // Handle memory allocation failure
        dst = NULL;
        return;
    }

    // Allocate memory for the content of the new string
    dest->content = (char *)malloc((src->length + 1) * sizeof(char));
    if (dest->content == NULL) {
        // Handle memory allocation failure
        string_cleanup(dest);
        dst = NULL;
        return;
    }

    // Copy the content of the source string
    strcpy(dest->content, src->content);

    // Update the length of the new string
    dest->length = src->length;

    dst = dest;
}

bool_t string_push_chars(string_t *self, char *chars) {
    int i = self->length;
    int j = 0;
    while (*(chars + j) != '\0') {
        // Calculate the new size of the string content, including the additional character
        char *tmp = realloc(self->content, (i + 1) * sizeof(char));
        if (tmp == NULL) {
            return FALSE;
        }
        self->content = tmp;
        *(self->content + i) = *(chars + j);
        i++;
        j++;
    }

    self->length = (int)chars_length(self->content) - 1;

    return TRUE;
}

bool_t string_push(string_t *self, string_t* to_be_pushed) {
    return string_push_chars(self, to_be_pushed->content);
}

compare_e string_compare(string_t* left, string_t *right) {
    int i = 0;
    while (
        *((left->content) + i) != '\0' || 
        *((right->content) + i) != '\0' 
    ) {
        if (*((left->content) + i) == *((right->content) + i)) {
            i++;
            continue;
        } else {
            return NOT_EQUAL;
        }
    }

    return EQUAL;
}

void string_print(string_t *self) {
    printf("%s", self->content);
}

void string_println(string_t *self) {
    printf("%s\n", self->content);
}

bool_t string_is_empty(string_t *self) {
    compare_e cmp = string_compare(self, string_from(""));
    switch (cmp) {
    case EQUAL:
        return TRUE;
    case NOT_EQUAL:
        return FALSE;
    default:
        return FALSE;
    }
}

void string_cleanup(string_t *self) {
    if (self != NULL) {
        free(self->content); // Free content
        free(self); // Free string_t structure
    }
}