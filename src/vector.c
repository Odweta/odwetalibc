#include <odweta/vector.h>
#include <odweta/util.h>
#include <odweta/list.h>
#include <odweta/string.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void vector_cleanup(vector_t *self) {
    free(self->data);
    free(self);
}

static int printNewline = 0;

void vector_reset(vector_t *self) {
    while (self->size > 0) {
        vector_pop(self);
    }
}

void _vector_print_recursive(vector_t *self, int depth) {
    int i;
    printf("[");
    for (i = 0; i < self->size; i++) {
        switch (self->type) {
            case INT:
                printf("%d", *((int *)(self->data) + i));
                break;
            case FLOAT:
                printf("%f", *((float *)(self->data) + i));
                break;
            case CHAR:
                printf("%c", *((char *)(self->data) + i));
                break;
            case VECTOR:
                // Recursively print each level of the vector
                _vector_print_recursive((vector_t *)(self->data + i * self->element_size), depth + 1);
                break;
            case LIST:
                _list_print_recursive((list_t *)(self->data + i * self->element_size), depth + 1);
            case BOOL:
                printf("%s", (*((bool_t *)(self->data + i * self->element_size)) == FALSE) ? "false" : "true");
                break;
            case STRING:
                string_print((string_t *)(self->data + i * self->element_size));
                break;
            default:
                //fprintf(stderr, "tried to print unsupported data type\n");
                //exit(EXIT_FAILURE);
                return;
        }

        if (i < self->size - 1 && self->type != CHAR) {
            printf(", ");
        } else if (self->type == CHAR && i < self->size - 2 && self->size != 1) {
            printf(", ");
        }
    }
    printf("]");
    if (depth != 0) {
        printf("\n");
    }
}

void vector_print(vector_t *self) {
    printf("[");
    int i;
    for (i = 0; i < self->size; i++) {
        switch (self->type) {
            case INT:
                printf("%d", *((int *)(self->data) + i));
                break;
            case FLOAT:
                printf("%f", *((float *)(self->data) + i));
                break;
            case CHAR:
                printf("%c", *((char *)(self->data) + i));
                break;
            case VECTOR:
                // Recursively print each level of the vector
                _vector_print_recursive((vector_t *)(self->data + i * self->element_size), 0);
                break;
            case LIST:
                _list_print_recursive((list_t *)(self->data + i * self->element_size), 0);
            case BOOL:
                printf("%s", (*((bool_t *)(self->data + i * self->element_size)) == FALSE) ? "false" : "true");
                break;
            case STRING:
                string_print((string_t *)(self->data + i * self->element_size));
                break;
            default:
                //fprintf(stderr, "tried to print unsupported data type\n");
                //exit(EXIT_FAILURE);
                return;
        }

        if (i < self->size - 1) {
            printf(", ");
        }
    }
    printf("]");
    if (printNewline == 0) {
        printf("\n");
    } else {
        printf("]");
    }
}

void _vector_print_as_string_recursive(vector_t *self, int depth) {
    int i;
    for (i = 0; i < self->size; i++) {
        switch (self->type) {
            case CHAR:
                printf("%c", *((char *)(self->data) + i));
                break;
            case VECTOR:
                // Recursively print each level of the vector
                _vector_print_as_string_recursive((vector_t *)(self->data + i * self->element_size), depth + 1);
                break;
            default:
                //fprintf(stderr, "tried to print a non-char type vector as string\n");
                //exit(EXIT_FAILURE);
                return;
        }
    }

    if (depth != 0) 
        printf("\n"); // do not print the last additional new line
}

void vector_print_as_string(vector_t *self) {
    switch (self->type) {
        case CHAR:
            for (int i = 0; i < self->size; i++) {
                printf("%c", *((char *)(self->data) + i));
            }
            printf("\n");
            break;
        case VECTOR:
            _vector_print_as_string_recursive(self, 0);
            break;
        default:
            //fprintf(stderr, "tried to print a non-char type vector as string | type: %d\n", self->type);
            //exit(EXIT_FAILURE);
            return;
    }
}

vector_t *vector_new(type_e type) {
    vector_t *self = malloc(sizeof(vector_t));
    if (self == NULL) {
        //fprintf(stderr, "failed to allocate memory for the vector\n");
        //exit(EXIT_FAILURE);
        return NULL;
    }

    self->type = type;
    self->size = 0;
    self->capacity = 1;
    switch (self->type) {
        case INT:
            self->element_size = sizeof(int);
            break;
        case FLOAT:
            self->element_size = sizeof(float);
            break;
        case CHAR:
            self->element_size = sizeof(char);
            break;
        case VECTOR:
            self->element_size = sizeof(vector_t);
            break;
        case LIST:
            self->element_size = sizeof(list_t);
            break;
        case BOOL:
            self->element_size = sizeof(bool_t);
            break;
        case STRING:
            self->element_size = sizeof(string_t);
            break;
        default:
            //fprintf(stderr, "specify a valid vector type\n");
            //exit(EXIT_FAILURE);
            return NULL;
    }

    self->data = malloc(self->element_size * self->capacity);
    if (self->data == NULL) {
        //fprintf(stderr, "failed to allocate memory for vector data\n");
        //exit(EXIT_FAILURE);
        return NULL;
    }

    return self;
}

void vector_deep_copy(vector_t *src, vector_t *dst) {
    if (src->type != dst->type) {
        // Handle type mismatch
        dst = NULL;
        return;
    }

    for (int i = 0; i < src->size; i++) {
        void *element_copy = NULL;

        switch (src->type) {
            case INT:
            case FLOAT:
            case CHAR:
            case BOOL:
                element_copy = malloc(sizeof(union { int x; float y; char z; bool_t w; }));  // Adjust size as needed
                memcpy(element_copy, (src->data) + i, src->element_size);
                break;
            case STRING:
                string_deep_copy((string_t *)(src->data + i * src->element_size), (string_t *)element_copy);
                break;
            case VECTOR:
                vector_deep_copy((vector_t *)(src->data + i * src->element_size), (vector_t *)element_copy);
                break;
            case LIST:
                list_deep_copy((list_t *)(src->data + i * src->element_size), (list_t *)element_copy);
                break;
        }
        
        if (element_copy == NULL) {
            vector_cleanup(dst);
            return;
        }

        // Push the deep-copied element into the destination vector
        vector_push(dst, element_copy);

        // Free the memory allocated for the element copy
        free(element_copy);
    }
}

void vector_push(vector_t *self, void* element) {
    void *tmp = realloc(self->data, (self->size + 1) * self->element_size);
    self->data = tmp;
    if (self->data == NULL) {
        //fprintf(stderr, "failed to allocate memory for vector data\n");
        //exit(EXIT_FAILURE);
        return;
    }

    switch (self->type) {
    case INT:
        *((int *)(self->data) + self->size) = *((int *)element);
        break;
    case FLOAT:
        *((float *)(self->data) + self->size) = *((float *)element);
        break;
    case CHAR:
        *((char *)(self->data) + self->size) = *((char *)element);
        break;
    case VECTOR:
        // Copy the vector data from the element to the vector
        vector_deep_copy((vector_t *)element, ((vector_t *)(self->data) + self->size));
        break;
    case LIST:
        list_deep_copy((list_t*)element, ((list_t *)(self->data) + self->size));
        break;
    case BOOL:
        *((bool_t *)(self->data) + self->size) = *((bool_t *)element);
        break;
    case STRING:
        string_deep_copy((string_t *)element, ((string_t *)(self->data) + self->size));
        break;
    default:
        return;
    }

    self->size += 1;
    if (self->size == self->capacity) {
        self->capacity *= 2;
    }
}

void *vector_pop(vector_t *self) {
    // capacity is always a power of 2, so a rounding error cannot happen i guess _/-(shi)-\_

    if (self->size != 0) {
        self->size -= 1;

        if (self->size <= self->capacity / 2) 
            self->capacity /= 2;

        if (self->capacity == 0) 
            self->capacity = 1;

        return (self->data) + self->size;
    } else {
        // cannot pop when there is no value in the vector
        return NULL;
    }
}

char *vector_to_string(vector_t *self) {
    char* ret_string;
    int total_size = 0;

    // Calculate the total size
    for (int i = 0; i < self->size; i++) {
        if (self->type == CHAR) {
            total_size += 1; // For a single character
        } else if (self->type == VECTOR) {
            vector_t *inner_vector = (vector_t *)vector_get_at(self, i);
            char *inner_string = vector_to_string(inner_vector);
            total_size += strlen(inner_string);

            // Add newline between vectors (except the last one)
            if (i < self->size - 1) {
                total_size += 1;
            }

            free(inner_string); // Free the memory allocated for innerString
        }
    }

    // Allocate memory for the string
    ret_string = (char *)malloc((total_size + 1 + 1) * sizeof(char));  // +1 for null terminator; +1 for ending '\n'

    // Copy the elements to the string
    int index = 0;
    for (int i = 0; i < self->size; i++) {
        if (self->type == CHAR) {
            ret_string[index++] = *((char *)vector_get_at(self, i));
        } else if (self->type == VECTOR) {
            vector_t *inner_vector = (vector_t *)vector_get_at(self, i);
            char *inner_string = vector_to_string(inner_vector);
            strcpy(ret_string + index, inner_string);
            index += strlen(inner_string);

            // Add newline between vectors (including the last one)
            if (i < self->size) {
                ret_string[index++] = '\n';
            }

            free(inner_string); // Free the memory allocated for innerString
        }
    }

    ret_string[index] = '\0';  // Null-terminate the string

    return ret_string;
}

void *vector_get_at(vector_t *self, int index) {
    return (self->data) + index;
}

void vector_set_at(vector_t *self, int index, void *element) {
    if (self->size == 0) {
        vector_push(self, element);
    } else if (index >= self->size) {
        return;
    }

    switch (self->type) {
        case INT:
            *((int *)(self->data) + index) = *((int *)element);
            break;
        case FLOAT:
            *((float *)(self->data) + index) = *((float *)element);
            break;
        case CHAR:
            *((char *)(self->data) + index) = *((char *)element);
            break;
        case VECTOR:
            memcpy((vector_t*)(self->data) + index, (vector_t*)element, sizeof(vector_t));
            break;
        case LIST:
            memcpy((vector_t*)(self->data) + index, (list_t*)element, sizeof(list_t));
            break;
        case BOOL:
            *((bool_t *)(self->data) + index) = *((bool_t *)element);
            break;
        case STRING:
            memcpy((vector_t*)(self->data) + index, (string_t*)element, sizeof(string_t));
            break;
        default:
            return;
    }
}

void vector_insert_at(vector_t *self, int index, void *element) {
    if (self->size == 0) {
        vector_push(self, element);
    } else if (index < self->size && index >= 0) {
        self->size += 1;
        if (self->size >= self->capacity) {
            self->capacity *= 2;
            void *tmp = realloc(self->data, self->element_size * self->capacity);
            self->data = tmp;
            if (self->data == NULL) {
                return;
            }
        }

        switch (self->type) {
            case INT:
                // move all elements to the right by one
                for (int i = self->size-1; i > index; i--) {
                    *((int *)(self->data) + i) = *((int *)(self->data) + i-1);
                }
                // set the element at the given index
                *((int *)(self->data) + index) = *((int *)element);
                break;
            case FLOAT:
                for (int i = self->size-1; i > index; i--) {
                    *((float *)(self->data) + i) = *((float *)(self->data) + i-1);
                }
                *((float *)(self->data) + index) = *((float *)element);
                break;
            case CHAR:
                for (int i = self->size-1; i > index; i--) {
                    *((char *)(self->data) + i) = *((char *)(self->data) + i-1);
                }
                *((char *)(self->data) + index) = *((char *)element);
                break;
            case VECTOR:
                for (int i = self->size-1; i > index; i--) {
                    memcpy((vector_t *)(self->data) + i, (vector_t *)(self->data) + i-1, sizeof(vector_t));
                }
                memcpy((vector_t*)(self->data) + index, (vector_t *)element, sizeof(vector_t));
                break;
            case LIST:
                for (int i = self->size-1; i > index; i--) {
                    memcpy((vector_t *)(self->data) + i, (list_t *)(self->data) + i-1, sizeof(list_t));
                }
                memcpy((vector_t *)(self->data) + index, (list_t *)element, sizeof(list_t));
                break;
            case BOOL:
                for (int i = self->size-1; i > index; i--) {
                    *((bool_t *)(self->data) + i) = *((bool_t *)(self->data) + i-1);
                }
                *((bool_t *)(self->data) + index) = *((bool_t *)element);
                break;
            case STRING:
                for (int i = self->size-1; i > index; i--) {
                    memcpy((vector_t *)(self->data) + i, (string_t *)(self->data) + i-1, sizeof(string_t));
                }
                memcpy((vector_t *)(self->data) + index, (string_t *)element, sizeof(string_t));
                break;
            default:
                return;
        }
    }
}

void vector_remove_at(vector_t *self, int index) {
    if (index < self->size && index >= 0 && self->size != 0) {    
        switch (self->type) {
            case INT:
                // move all elements to the right by one
                for (int i = index; i < self->size; i++) {
                    *((int *)(self->data) + i) = *((int *)(self->data) + i+1);
                }
                break;
            case FLOAT:
                for (int i = index; i < self->size; i++) {
                    *((float *)(self->data) + i) = *((float *)(self->data) + i+1);
                }
                break;
            case CHAR:
                for (int i = index; i < self->size; i++) {
                    *((char *)(self->data) + i) = *((char *)(self->data) + i+1);
                }
                break;
            case VECTOR:
                for (int i = index; i < self->size; i++) {
                    memcpy((vector_t *)(self->data) + i, (vector_t *)(self->data) + i+1, sizeof(vector_t));
                }
                break;
            case LIST:
                for (int i = index; i < self->size; i++) {
                    memcpy((vector_t *)(self->data) + i, (list_t *)(self->data) + i+1, sizeof(list_t));
                }
                break;
            case BOOL:
                for (int i = index; i < self->size; i++) {
                    *((bool_t *)(self->data) + i) = *((bool_t *)(self->data) + i+1);
                }
                break;
            case STRING:
                for (int i = index; i < self->size; i++) {
                    memcpy((vector_t *)(self->data) + i, (string_t *)(self->data) + i+1, sizeof(string_t));
                }
                break;
            default:
                return;
        }

        self->size -= 1;
        if (self->size < self->capacity/2) {
            self->capacity /= 2;
            void *tmp = realloc(self->data, self->element_size * self->capacity);
            self->data = tmp;
            if (self->data == NULL) {
                return;
            }
        }
    } else {
        return;
    }
}
