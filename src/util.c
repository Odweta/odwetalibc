#include <odweta/util.h>
#include <odweta/vector.h>

char *int_to_string(int x) {
    vector_t *string = vector_new(CHAR);

    if (x > 0 && x < 10) {
        vector_push(string, &(char){x+48});
        char *retval = vector_to_string(string);
        vector_cleanup(string);
        return retval;
    } else {
        while (x > 0) {
            int last_digit = x % 10;
            x /= 10;
            vector_insert_at(string, 0, &(char){last_digit+48});
        }
        vector_pop(string);
        char* retval = vector_to_string(string);
        vector_cleanup(string);
        return retval;
    }
}

long pow(int x, int n) {
    long retval = 1;

    for (int i = 0; i < n; i++) {
        retval *= x;
    }

    return retval;
}

int max_lower_power_of_10(int x) {
    int i = 0;
    long prev = pow(10, i++);

    while (1) {
        long now = pow(10, i);
        if (now >= x) {
            return (int)prev;
        }
        prev = now;
        i++;
    }
}
