#include <odweta/vector.h>
#include <stdio.h>

int main() {
    printf("creating a vector using 'vectorInit(<type>)' (returns a vector_t*)\npossible type are: INT, FLOAT, CHAR, VECTOR (LIST in the future)\n");
    vector_t* v = vectorInit(INT);

    printf("pushing an integer value into the vector\nnote the kind of unusual way of passing the number argument: if you want to pass an expression (e.g. the value '1' or '4*4'), something not stored in a variable, you follow this syntax - &(type){value} (e.g. &(int){1 + 1})\n->'vectorPush(vector_t*, &(int){123})'\n");
    vectorPush(v, &(int){123});
    vectorPrint(v);

    printf("popping the last value of the vector; in this case, it is an integer, so you type: 'int poppedValue = *((int *)vectorPop(vector_t*))'\n");
    int poppedValue = *((int *)vectorPop(v));
	printf("popped: %d\n", poppedValue);
    vectorPrint(v);

    printf("if you are finished working with the vector BEFORE the immediate end of the program, run 'vectorCleanup(vector_t*)', otherwise it is not strictly necessary\n");
    vectorCleanup(v);

    return 0;
}
