#include <odweta/string.h>

int main() {
    //printf("before init\n");
    string_t *s = string_from("test string");

    //printf("before 1st println\n");
    string_println(s);

    //printf("before pushing chars\n");
    string_push_chars(s, " appended");

    //printf("before 2nd println\n");
    string_println(s);

    //printf("before cleanup\n");
    string_cleanup(s);

    //printf("before end\n");
    return 0;
}
