// #include <stdio.h>
// #include <odweta/util.h>
//
// int main() {
//     char* string = intToString(1729843);
//
//     printf("final string: %s\n", string);
//
//     char* sstring = intToString(123);
//
//     printf("final string: %s\n", sstring);
//
//     return 0;
// }

#include <odweta/file.h>
#include <stdio.h>

int main() {
    printf("creating a file using 'fileInit(<filename>)' (returns a file_t*)\n");
    file_t* f = fileInit("test.txt");

    printf("printing the file contents\n\n");
    filePrint(f);

    printf("\nconvering the file contents into a string (char*)\n");
    char* string = fileToString(f);
    printf("\"\"\"\n%s\"\"\"\n", string);

    printf("writing is quite simple, just call the 'fileWrite(file_t*, char*)' or 'fileWriteln(file_t*, char*)' function, where the second argument is the string you want to write into a file\nthe 'fileWriteln()' function writes the specified string AND a '\\n' after it, whereas the 'fileWrite()' function does not append the '\\n'\n");
	fileWrite(f, "'this will not end with a \"\\n\"' ");
	fileWriteln(f, "this line was appended");
	filePrint(f);

    printf("if you are finished working with the file BEFORE the immediate end of the program, run 'fileCleanup(file_t*)', otherwise it is not strictly necessary\nit will, though, append a '\\n' at the end of the file if you have not done so, making writing into files more consistent, so for this reason, I recommend you use it with every file_t*\n");
    fileCleanup(f);

    printf("copying and moving\nafter this block of code, the contents of test.txt will have been copied into copy_of_test.txt and moved to moved_copy_of_test.txt, which means that copy_of_test.txt is deleted and the contents of test.txt are only in test.txt and moved_copy_of_test.txt\n");

    file_t* src = fileInit("test.txt");

    fileCopy(src, "copy_of_test.txt");

    printf("src:\n");
    filePrint(src);

    printf("\ndst:\n");
    file_t* dst = fileInit("copy_of_test.txt");
    filePrint(dst);

    fileMove(dst, "moved_copy_of_test.txt");

    printf("moved file:\n");
    file_t* move_dst = fileInit("moved_copy_of_test.txt");
    filePrint(move_dst);

    fileCleanup(src);
    printf("DO NOT cleanup a deleted file! (dst, in this case) it is already done for you by the 'fileMove(file_t*, char*)' function\n");
    // fileCleanup(dst);
    fileCleanup(move_dst);

    printf("when copying and moving files, the first argument is a file_t* (source file) and the second argument is a char* (the path of the destination file), not a file_t*!\n");

    printf("prepending line numbers at the beggining of each line in a file\n");

    file_t* g = fileInit("test.txt");
    fileCopy(g, "copy_of_test.txt");
    file_t* g_copy = fileInit("copy_of_test.txt");
    fileNumberLines(g_copy);

    printf("file withOUT prepended numbers:\n");
    filePrint(g);

    printf("file with prepended numbers:\n");
    filePrint(g_copy);

    fileCleanup(g);
    //fileCleanup(g_copy);

    printf("counting words in a file\n");

    file_t* words = fileInit("test.txt");

    int word_count = fileCountWords(words);
    printf("word count of file %s is %d\n", words->path, word_count);

    fileCleanup(words);

    return 0;
}
