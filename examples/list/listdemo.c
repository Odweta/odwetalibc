#include <odweta/list.h>
#include <odweta/vector.h>
#include <stdio.h>

int main() {
    printf("creating a list using 'listInit()' (returns a list_t*)\n");
    list_t* l = listInit();

    printf("pushing an integer value into the list\nnote the kind of unusual way of passing the number argument: if you want to pass an expression (e.g. the value '1' or '4*4'), something not stored in a variable, you follow this syntax - &(type){value} (e.g. &(int){1 + 1})\nalso, the type of the variable must be given as the second argument to the function\n->'listPush(list_t*, INT, &(int){123})'\n");
    listPush(l, INT, &(int){123});
    listPrint(l);

    printf("popping the last value of the list; in this case, it is an integer, so you type: 'int poppedValue = *((int *)listPop(list_t*))'\n");
    int poppedValue = *((int *)listPop(l));
    printf("popped: %d\n", poppedValue);
    listPrint(l);

    printf("more examples with diferrent types\nnote that the vector_t* is a pointer, so there is no need to write the special syntax!\n");
    listPush(l, FLOAT, &(float){3.14});
    listPush(l, CHAR, &(char){'x'});
    vector_t* int_vec = vectorInit(INT);
    vectorPush(int_vec, &(int){456});
    listPush(l, VECTOR, int_vec);

    listPrint(l);

    printf("inserting at index 1\n");
    listInsertAt(l, 1, FLOAT, &(float){6.67});

    listPrint(l);

    printf("if you are finished working with the list BEFORE the immediate end of the program, run 'listCleanup(list_t*)', otherwise it is not strictly necessary\n");
    listCleanup(l);

    return 0;
}
