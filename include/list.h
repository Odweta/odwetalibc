#ifndef LIST_H
#define LIST_H

/*
 * What is self?
 *
 * A list (as I interpret it in self library)
 * is a `vector_t` of other `vector_t`s, where
 * every inner `vector_t` can be of any type ->
 * self means a Python-like list
 */

#include <odweta/vector.h>
#include <odweta/util.h>

typedef struct {
    size_t size;            // number of elements that are inside the list
    size_t capacity;        // how many elements can be put inside
    vector_t *data;         // vector of vectors
} list_t;

list_t *list_new();                                                            // returns a new list

void list_push(list_t *self, type_e type, void *element);                      // appends an element at the end of a list

void list_show(list_t *self);                                                  // prints a list
void _list_print_recursive(list_t *self, int depth);

void list_cleanup(list_t *self);                                               // freeing pointers

void *list_pop(list_t *self);                                                  // popping the last value of the list and returning it

void list_deep_copy(list_t *src, list_t *dst);

vector_t *list_get_at_vector(list_t *self, int index);

void list_insert_at(list_t *self, int index, type_e type, void *element);      // inserts an element at a given index

void list_remove_at(list_t *self, int index);

void list_set_at(list_t *self, int index, type_e type, void *element);

void *list_get_at(list_t *self, int index);

#endif // LIST_H