#ifndef FILE_H
#define FILE_H

#include <odweta/vector.h>
#include <odweta/util.h>

typedef struct {
	char *path;								// the file descriptor
	int length;								// line count
	vector_t *lines;						// 2d array of chars
	int not_newline_terminated;				// bool: 1 = true, 0 = false
} file_t;

file_t *file_new(char *path);	     			// initialization function

void file_cleanup(file_t *self);					// just to be memory safe...

void file_print(file_t *self);						// prints the file as a string

char *file_to_string(file_t *self);					// returns a string with the file contents

void file_writeln(file_t *self, char *payload);		// appends a given string to the file on a newline

void file_write(file_t *self, char *payload);		// appends a given string to the file (not on a newline!)

void file_copy(file_t* src, char *dst);			// copy file src into file dst

void file_delete(file_t *self);						// deletes a file

void file_move(file_t* src, char *dst);			// moves/renames a file

bool_t file_exists(file_t *self);						// TRUE = yes, FALSE = no

void file_create(char *path);				// create an empty file with the given name

void file_number_lines(file_t *self);				// prepend line numbers in front of each line in a file

int file_count_words(file_t *self);					// counts the number of words in a file

#endif // FILE_H
