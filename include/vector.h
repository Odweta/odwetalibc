#ifndef VECTOR_H
#define VECTOR_H

#include <odweta/util.h>

typedef struct {
	type_e type;
	size_t size;
	size_t capacity;
	size_t element_size;
	void *data;
} vector_t;

vector_t *vector_new(type_e type); 						// size gets determined by the dataType and the capacity starts at 1 and x1.5 every time the vector gets filled up

void vector_reset(vector_t *self);						// sets the vector to its original state

void *vector_pop(vector_t *self);						// remove one value from the end of the vector and return it

void vector_push(vector_t *self, void *element);		// push one value at the end of the vector

void vector_cleanup(vector_t *self); 					// freeing pointers

void vector_print(vector_t *self); 						// shows the vector

void vector_print_as_string(vector_t *self);			// show the vector as a string if the v->type is CHAR

void vector_insert_at(vector_t *self, int index, void *element); 	// inserts an element 'element' at index 'index'

void vector_set_at(vector_t *self, int index, void *element);		// set an element's value at a given index

char *vector_to_string(vector_t *self);							// convert the vector into a regular string

void *vector_get_at(vector_t *self, int index);					// get an element at the given index

void vector_remove_at(vector_t *self, int index);

void vector_deep_copy(vector_t *src, vector_t *dst);



void _vector_print_recursive(vector_t *self, int depth);

#endif
