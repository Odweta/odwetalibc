#ifndef UTIL_H
#define UTIL_H

#undef bool

typedef enum { FALSE, TRUE } bool_t;

typedef enum {
	LESS,
	LESS_EQUAL,
	EQUAL,
	NOT_EQUAL,
	GREATER,
	GREATER_EQUAL
} compare_e;

typedef long unsigned int size_t;

typedef enum {
	INT,
	FLOAT,
	CHAR,
	VECTOR, 						// allows for multi-dimensional arrays
	LIST,
	BOOL,
	STRING
} type_e;

char* int_to_string(int x);         // convert an int into a string

long pow(int x, int n);             // x^n

int max_lower_power_of_10(int x);	// biggest power of 10 that is < x

#endif // UTIL_H
