#ifndef STRING_H
#define STRING_H

#include <odweta/util.h>

typedef struct {
    char *content;
    size_t length;
} string_t;

string_t *string_new();                                        // create a new empty string

string_t *string_from(char *chars);                            // create a new string from a given char_sequence

bool_t string_push_chars(string_t *self, char *chars);           // append a char_sequence to a string
bool_t string_push(string_t *self, string_t *other);             // append a string to a string

void string_deep_copy(string_t *src, string_t *dst);

compare_e string_compare(string_t *left, string_t* right);     // compare two strings

void string_print(string_t *self);                             // print a string
void string_println(string_t *self);                           // print a string with a \n

bool_t string_is_empty(string_t *self);                          // return true when s->content == ""

void string_cleanup(string_t *self);                           // memory cleanup

#endif