#include <stdio.h>
#include <stdlib.h>
#include <odweta/vector.h>
#include <odweta/string.h>

int main() {
    vector_t *v = vector_new(INT);

    vector_push(v, &(int){33});
    vector_push(v, &(int){35});
    vector_push(v, &(int){34});

    vector_print(v);

    vector_cleanup(v);

    string_t *s = string_new();

    string_println(s);

    string_push_chars(s, "this is a test");

    string_println(s);

    compare_e cmp = string_compare(s, string_from("this is a test"));

    printf("cmp -> %s\n", (cmp == EQUAL) ? "equal" : "not equal");

    string_cleanup(s);

    return 0;
}
