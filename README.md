# OdwetaLibC

I got bored with having to use complicated checking and stuff when working with files and arrays in C, so I implemented a new `file_t` struct which can load the contents of a file into a dynamically allocated array (yes, there is an implementation of those here as well... (`vector_t` struct)). The struct also stores the number of lines the file has.

Also, I managed to come up with an implementation of a kind-of-Python-like list (a vector of vectors of any type interpreted as just one vector). Quite handy, I'd say. The `list_t` struct is what you want to use when working with lists.

The difference between a `vector_t` and a `list_t` is that a vector can only be of one type, whereas a list can have values of any type inside it (in this context, any means types defined in this library and default types in C).

Examples of this work of art are in the `/examples/` directory of this repo.

To create a project that uses this library easier, use the `vendetta` utility (`https://gitlab.com/Odweta/vendetta`), otherwise it can be difficult to link all the required libraries.

## Files

#### What works so far:

1. Reading files into a 2D vector
2. Converting a file into a string
3. File writing logic
4. Copying a file to another file
5. Moving (renaming) a file to another file
6. Deleting a file
7. Checking if a file exists
8. Add line numbers at the beggining of each line in a file (with correct indentation)
9. Number of words in a file

#### TODO:

+ You tell me

## Vectors (dynamic arrays)

#### What works so far:

1. Dynamically allocated arrays (vectors)
2. Converting a vector into a string (has to be a `char` vector)
3. Popping the last value of a vector and returning it
4. Getting and setting a value of a vector on a given index
5. Pushing a value at the end of a vector
6. Inserting a value into a vector at a given index
7. Insertion of a vector into another vector
8. Vectors of lists
9. Remove a value at a given index

#### TODO:

+ You tell me

## Lists (vectors with dynamic types)

#### What works so far:

1. Creating a list
2. Pushing into a list
3. Printing a list
4. Popping the last value of a list
5. Cleanup of a list
6. Vectors inside of a list
7. Inserting at a given index
8. Lists of lists
9. Remove at a given index
10. Get/set at a given index

#### TODO:

+ You tell me

## Installation

`git clone https://gitlab.com/Odweta/odwetalibc.git && cd odwetalibc && make install`

#### Run demo

(the library has to be installed with `make install`)

`make && ./demo`

<hr>
<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://gitlab.com/Odweta/odwetalibc">OdwetaLibC</a> by <a rel="cc:attributionURL dct:creator" property="cc:attributionName" href="https://gitlab.com/Odweta">Odweta</a> is licensed under <a href="http://creativecommons.org/licenses/by-nc-nd/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-NC-ND 4.0<br><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nd.svg?ref=chooser-v1"></a></p>
