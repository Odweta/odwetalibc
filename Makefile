CC := gcc
CFLAGS := -Wall -Werror
LIBS := -lvector -lfile -llist -lutil -lstring
LIBSPATH := -L/usr/lib/odweta/
INCLUDEDIR := -I/usr/include/odweta/
PATCH := -Wl,-rpath=/usr/lib/odweta/

.PHONY: default install

default: demo

demo: demo.c libvector.so libfile.so liblist.so libutil.so
	sudo $(CC) $(CFLAGS) -o $@ $< $(PATCH) $(INCLUDEDIR) $(LIBSPATH) $(LIBS)

trydemo:
	@make > /dev/null
	@make install > /dev/null
	@make > /dev/null
	./demo
	@make cleanrepo > /dev/null

libvector.so: src/vector.c
	sudo $(CC) -c -fpic $< -o /usr/lib/odweta/vector.o
	sudo $(CC) -shared -o /usr/lib/odweta/$@ /usr/lib/odweta/vector.o

libfile.so: src/file.c
	sudo $(CC) -c -fpic $< -o /usr/lib/odweta/file.o
	sudo $(CC) -shared -o /usr/lib/odweta/$@ /usr/lib/odweta/file.o

liblist.so: src/list.c
	sudo $(CC) -c -fpic $< -o /usr/lib/odweta/list.o
	sudo $(CC) -shared -o /usr/lib/odweta/$@ /usr/lib/odweta/list.o

libutil.so: src/util.c
	sudo $(CC) -c -fpic $< -o /usr/lib/odweta/util.o
	sudo $(CC) -shared -o /usr/lib/odweta/$@ /usr/lib/odweta/util.o

libstring.so: src/string.c
	sudo $(CC) -c -fpic $< -o /usr/lib/odweta/string.o
	sudo $(CC) -shared -o /usr/lib/odweta/$@ /usr/lib/odweta/string.o

install:
	sudo mkdir -p /usr/include/odweta/
	sudo mkdir -p /usr/lib/odweta/
	sudo cp src/vector.c src/file.c src/list.c src/util.c src/string.c include/vector.h include/file.h include/list.h include/util.h include/string.h /usr/include/odweta/
	sudo make libvector.so libfile.so liblist.so libutil.so libstring.so
	sudo ldconfig # update ldconfig cache

clean:
	sudo rm -f main /usr/lib/odweta/vector.o /usr/lib/odweta/file.o /usr/lib/odweta/list.o /usr/lib/odweta/util.o /usr/lib/odweta/string.o /usr/lib/odweta/libvector.so /usr/lib/odweta/libfile.so /usr/lib/odweta/liblist.so /usr/lib/odweta/libutil.so /usr/lib/odweta/libstring.so

cleanrepo:
	rm -fr ./demo ./moved_copy_of_test.txt ./copy_of_test.txt
